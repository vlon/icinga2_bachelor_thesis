# Setup and Implementation of Icinga 2 as a Monitoring System

This thesis describes the setup and implementation of the
monitoring system Icinga 2. Monitoring of the infrastructure
in a network is a critical part of system administration
and ensures that companies are able to deliver continuously
available services to their customer base. This
report describes in detail the prosess of upgrading the
monitoring solution Icinga 1 to Icinga 2 as well as the
accompanying configuration and software.

Authors:

* Vemund Lonbakken

* Gaute Holm Runningshaugen

* Dan-Jimmy Hogstad